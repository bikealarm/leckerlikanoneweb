<?php

require_once 'classes/Membership.php';
$membership = New Membership();

$membership->confirm_Member();

?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="height = 960, width = 1000px user-scalable = yes">
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
<div id="main">
	<div id="header">
		<div id="logo">
			<img src="images/header.png">
		</div>
		<div id="nav">
			<ul id="menu">
			  <!-- put class="selected" in the li tag for the selected page - to highlight which page you're on -->
			  <li><a href="index.php">Home</a></li>
			  <li><a href="settings.php">Settings</a></li>
			  <li><a href="stats.php">Stats</a></li>
			  <li class="selected"><a href="about.php">About</a></li>
			  <li><a href="help.php">Help</a></li>
			  <li><a href="login.php?status=loggedout">Log Out</a></li>
			</ul>
		</div>
	</div>


	<div id="content">
		<div class = "sidebar">
			<figure>
				<img src="images/CADModell2.PNG" width="230" height="200" alt="CADModell">
				<figcaption><b>CAD-Modell</b>, Übersicht zur Funktionsweise und Aufbau der Hundekanone
				</figcaption>
			</figure>
			<br>
			
			<figure>
				<img src="images/Spanner.png" width="230" height="200" alt="Spanner">
				<figcaption><b>Spanner Motor</b>, zum Spannen des Gummis
				</figcaption>
			</figure>
			<br>
			
			<figure>
				<img src="images/Halter.png" width="230" height="200" alt="Halter">
				<figcaption><b>Halter Motor</b>, zum Festhalten des Wurfarms
				</figcaption>
			</figure>
			<br>
			
			<figure>
				<img src="images/Nachladen.png" width="230" height="200" alt="Nachladen">
				<figcaption><b>Nachlademechanismus</b>, Leckerlis rutschen nach sobald eins hinuntergestoßen wurde
				</figcaption>
			</figure>
			<br>
			<figure>
				<img src="images/GesamtSystem.jpg" width="230" height="260" alt="Gesamtkonstruktion">
				<figcaption><b>Gesamtkonstruktion</b>
				</figcaption>
			</figure>
		</div>
		<div id="content_text">
			<p>
				Das Projekt "Leckerlie Kanone" wurde als Fallstudie an der Hochschule Fulda bearbeitet. 
				Es wurde von Tobias Kalb und Timo Weber bearbeitet, Studenten der Elektro- und Informationstechnik 
				mit der Vertiefung <I>Computer Engineering</I>.
				
			</p>
			<h1>Motivation</h1>
				<p>
					Haustiere werden tagsüber, während die Herrchen außer Haus sind, alleine zurückgelassen.
					Dies führt zu Langeweile, Einsamkeit und möglicherweise 
					unerwünschten Verhaltensweisen. Generell sind Spielzeuge eine gute Idee, um einem 
					Haustier das Alleinsein zu versüßen. Neben dem Bereitstellen von Spielzeugen und der 
					Herrichtung einer artgerechten Wohnung, wird beispielsweise Katzenbesitzern geraten, so genannte 
					„Futterbeschäftigungen“ für	die Katze zu finden. Hausschild empfiehlt, dass zumindest
					ein Teil der täglichen Futter- und Snackration so angeboten werden sollte, dass eine Katze
					zu zusätzlichen Aktivitäten veranlasst werde. Beispielsweise solle Futter in der Wohnung 
					versteckt werden oder in ein Intelligenz-Spielzeug integriert werden, bei denen die 
					Tiere Leckerli aus einem Spielball oder Spielbrett fummeln müssen.
				</p>
			
			<h1>Idee</h1>
				<p>
					Haustierspielzeuge sind jedoch meist nicht sehr abwechslungsreich und regen das Haustier
					nicht dazu an, sich zu bewegen. Der Gang durch die Wohnung auf der Suche nach den versteckten
					Leckerlis erfolgt meist nur Sekunden, nachdem das Herrchen das Haus verlassen hat,
					so dass das Haustier für den restlichen Arbeitstag des Herrchens nicht mehr zur 
					Bewegung animiert wird.
					Eine bisher weitestgehend übersehene Lösung ist eine Leckerli-Kanone, die von dem Herrchen über
					eine Web-Interface von der Arbeit oder Unterwegs gesteuert werden kann. Ein von einer
					solchen Kanone geworfenes Leckerli oder Trockenfutterstück regt den natürlichen Jagdtrieb
					bspw. einer Katze an oder kann als Futterration des Haustiers dienen.
				</p>
			
			
			<h1>Umsetzung</h1>
			<p>
				<OL>
					<LI>Schieß/Wurfmechanismus für Snacks (siehe rechts)
					<LI>Steuerung mithilfe eines Raspberry Pi 2 und 3
					<LI>Anbindung an das Internet über Ethernet/WLAN
						<UL TYPE=disc>
							<LI>Raspberry Pi dient als Apache HTTP Server
						</UL>
					<LI>Steuerung der Snack-Kanone über html Web-Interface
						<UL TYPE=disc>
							<LI>Mithilfe der <I>RPi.GPIO Python Library</I>
						</UL>
					<LI>Kamera zur Beobachtung des Haustiers → ermöglicht Online Streaming des Bildes
						<UL TYPE=disc>
							<LI>Umsetzung mit  <I>mjpg-streamer</I>
						</UL>
					<LI>Lautsprecher um dem Haustier zu signalisieren, dass ein Snack verschossen wird
				</OL>
					<br>
				<B>Weitere Eigenschaften:</B>
				<UL TYPE=disc>
					<LI>Sicherheit des Systems vor unbefugtem Zugriff → Zugang nur durch Benutzername und Passwort
					<LI>Das Haustier darf sich unter keinen Umständen an dem Gerät verletzen können
					<LI>Eine Anwendung für Hunde und eine für Katzen wurde erarbeitet
				</UL>	
				<br>
				Weitere Informationen kann man der angehängten Dokumentation entnehmen:
				<a href="pdf/document.pdf" download="Dokumentation">Download!</a>
			</p>
			
			<br><br><br>
		</div>
		
		
	</div>
	<div id="content_footer"></div>
		<div id="footer">
				<a href="http://validator.w3.org/check?uri=referer">HTML5</a> |
				<a href="http://jigsaw.w3.org/css-validator/check/referer">CSS</a> |
				<a href="http://www.html5webtemplates.co.uk">design from HTML5webtemplates.co.uk</a>
		
			<br><br>
			<p class="right">Teammitglieder: Timo Weber | Tobias Kalb</p>
		</div>
	</div>
</div>

</body>
</html>

