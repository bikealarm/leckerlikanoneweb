<?php

require_once 'classes/Membership.php';
$membership = New Membership();

$membership->confirm_Member();

?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="height = 960, width = 1000px user-scalable = yes">
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
<div id="main">
	<div id="header">
		<div id="logo">
			<img src="images/header.png">
		</div>
		<div id="nav">
			<ul id="menu">
			  <!-- put class="selected" in the li tag for the selected page - to highlight which page you're on -->
			  <li class="selected"><a href="index.php">Home</a></li>
			  <li><a href="settings.php">Settings</a></li>
			  <li><a href="stats.php">Stats</a></li>
			  <li><a href="about.php">About</a></li>
			  <li><a href="help.php">Help</a></li>
			  <li><a href="login.php?status=loggedout">Log Out</a></li>
			</ul>
		</div>
	</div>


	<div id="content">
		<div id="controls">
			<table>
				<tr>
					<td><button class="buttonStyle" type="button" onclick="shoot(this)">Shoot</button></td>
				</tr>
				<tr>
					<td><button class="buttonStyle" type="button" onclick="play(this)">Play Sound</button></td>
				</tr>
		
				<tr>
					<td><button id="camera_power" class="buttonStyle" type="button" onclick="camOFF()" >Camera Off</button></td>
				</tr>
				<tr>
					<td><label for="Angle">Strength</label>
						<input type="range" id="strength" value="100" onchange="showValue(this, this.value)" />
						<span id="range">100%</span>
						<script type="text/javascript">
						function showValue(el, newValue)
						{
							document.getElementById("range").innerHTML=newValue + "%";
							var Schussstaerke = newValue;
						}
						</script>
					</td>
				</tr>
				<tr>
					<td><button class="buttonStyle" type="button" onclick="turnSystemOff()" ><img src="images/powerbutton.png" width="20px"/></button></td>
				</tr>
			</table>
		</div>
		<div id="stream">
			<img src="http://192.168.2.106:8080/?action=stream" onerror="buttonturn(this)"/>
		</div>


	</div>
	
	<div id="content_footer"></div>
		<div id="footer">
				<a href="http://validator.w3.org/check?uri=referer">HTML5</a> |
				<a href="http://jigsaw.w3.org/css-validator/check/referer">CSS</a> |
				<a href="http://www.html5webtemplates.co.uk">design from HTML5webtemplates.co.uk</a>
		
			<br><br>
			<p class="right">Teammitglieder: Timo Weber | Tobias Kalb</p>
		</div>
	</div>
</div>
<script src="js/jquery.min.js"></script>
<script type="text/javascript">

 function buttonturn (el) {
	var Id = "camera_power";
	if (el.src != 'images/ripStream.jpg') {
		el.src = 'images/ripStream.jpg';
	}
	var butt = document.getElementById("camera_power");
	butt.firstChild.data = "Camera ON";
	//document.getElementById(Id+"Button").onclick = function() {camON();}
	butt.setAttribute("onclick", "javascript: camON();");
	
 }

 function shoot (el) {
	 var Schussstaerke = document.getElementById("strength").value/100.0;
	 el.disabled = true;
	  $.ajax({
		url:"php/shootServo.php", //the page containing php script
		type: "POST", //request type
		data: { Schussstaerke : Schussstaerke},
		success:function(result){
			el.disabled = false;
		}
	 });
 }
  function play (el) {
	  el.disabled = true;
	  $.ajax({
		url:"php/sound.php", //the page containing php script
		type: "POST", //request type
		success:function(result){
			el.disabled = false;
	   }
	 });
 }
  function camOFF () {
	  $.ajax({
		url:"php/camOFF.php", //the page containing php script
		type: "POST", //request type
		success:function(result){
			location.reload();
	   }
	 });
 }
  function camON () {
	  $.ajax({
		url:"php/camON.php", //the page containing php script
		type: "POST", //request type
		success:function(result){
			location.reload();
	   }
	 });
 }
 
function turnSystemOff() {
	if (confirm("Möchten Sie die Leckerli-Kanone wirklich herunterfahren?") == true) {
		$.ajax({
			url:"php/turnSystemOff.php", //the page containing php script
			type: "POST", //request type
			success:function(result){}
		});
	}
}
</script>
</body>
</html>

