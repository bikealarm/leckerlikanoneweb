#!/usr/bin/python

import StepperFunctions as Step


Step.createStepper("Shooter", [32, 36, 38, 40], 0, 0)
Step.createStepper("Loader", [31, 33, 35, 37], 0, 0)
Step.createStepper("Turner", [15, 13, 11, 7], 0, 0)