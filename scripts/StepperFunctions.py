#!/usr/bin/python
import RPi.GPIO as GPIO
import time
import sys
import os
import re
from ConfigParser import ConfigParser

steppersDir = os.path.abspath(os.path.dirname(__file__)) + "/steppers"
configFilePath = steppersDir + "/motors.ini"
stepFull = 512
motorPins = [0,0,0,0]
motorPos = 0
lastStep = stepFull
maxWinkel = 0

if not os.path.exists(steppersDir):
		os.makedirs(steppersDir)

seql = [ [1,1,0,0],
		[0,1,1,0],
		[0,0,1,1],
		[1,0,0,1] ]


seq = [ [1,0,0,1],
		[0,0,1,1],
		[0,1,1,0],
		[1,1,0,0] ]

def convToIntArray(str):
	return map(int, re.findall('\d+', str))

def calculateTargetPos(targetAngle):
	return int(float(targetAngle) / 360 * stepFull)
	
def setupStepper(name): 
	readStepperFile(name)
	GPIO.setmode(GPIO.BOARD)
	if (len(motorPins)== 4):
		for pin in motorPins:
			GPIO.setup(pin, GPIO.OUT)
			GPIO.output(pin, 0)
			
def createStepper(name, ControlPins, currentPos, maxWinkel):
	config = ConfigParser()
	config.read(configFilePath)
	if not config.has_section(name):
		print "A new section has been added."
		config.add_section(str(name))
	
	config.set(name,"Pins", str(ControlPins))
	config.set(name, "currentPos", str(currentPos))
	config.set(name, "maxWinkel", str(maxWinkel))
	with open(configFilePath, 'w') as configfile:
		config.write(configfile)
		configfile.close()

def readStepperFile(name):
	global motorPins
	global motorPos
	global maxWinkel
	config = ConfigParser()
	config.read(steppersDir + "/motors.ini")
	motorPos = config.getint(name, "currentPos")
	motorPins = convToIntArray(config.get(name, "Pins"))
	maxWinkel = config.getint(name, "maxWinkel")
	lastStep = int(stepFull * float(maxWinkel) / 360)


def changeStepperPos(name, currentPos):

	config = ConfigParser()
	config.read(configFilePath)

	config.set(name, "currentPos", str(currentPos))
	with open(configFilePath, 'w') as configfile:
		config.write(configfile)
		configfile.close()

def turnStepperSteps(name, steps):

	setupStepper(name)
	newPosition = 0
	deltaPerStep = 1
	if(steps < 0):
		steps = int(abs(steps))
		seqHERE = seql
		deltaPerStep = -1
		
	else:
		seqHERE = seq
	
	for i in range(steps):
		for halfstep in range(4):
			for pin in range(4):
				GPIO.output(motorPins[pin], seqHERE[halfstep][pin])
			time.sleep(0.005)
		newPosition += deltaPerStep
	newPosition = newPosition + motorPos
	while(newPosition >= stepFull):
		newPosition = newPosition - stepFull
	changeStepperPos(name, newPosition)
	GPIO.cleanup()
		
def turnStepperAngle(name, angle):
	readStepperFile(name)
	if maxWinkel == 0:
		targetPos = calculateTargetPos(angle)
		turnStepperSteps(name, targetPos)
	else:
		print "Cannot use turnStepperAngle when maxWinkel is set."

def turnToPos(name, targetAngle):
	readStepperFile(name)
	if maxWinkel == 0:
		
		targetPos = calculateTargetPos(targetAngle)
		deltaPos = motorPos - targetPos
		turnStepperSteps(name, deltaPos)
	else:
		print "Cannot use turnToPos when maxWinkel is set."

def turnToInRange(name, targetAngle):
	readStepperFile(name)
	deltaPos = 0
	
	if not maxWinkel == 0:
		if(targetAngle <= maxWinkel) and (targetAngle >= 0):
			deltaPos = calculateTargetPos(targetAngle) - motorPos
			turnStepperSteps(name, deltaPos)
		else:
			print("targetAngle exceeds the motor turn range.")
	else: 
		print "maxWinkel is not set. Cannot use turnInRange"