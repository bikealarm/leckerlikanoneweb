#!/usr/bin/python

import RPi.GPIO as GPIO
import time
import sys
import os
if (len(sys.argv) == 3):
	GPIO.setmode(GPIO.BOARD)
	
	ControlPin3 = [15, 13, 11, 7]
	ControlPin2 = [31, 33, 35, 37]
	ControlPin1 = [32, 36, 38, 40]
	
	seql = [ [1,1,0,0],
			[0,1,1,0],
			[0,0,1,1],
			[1,0,0,1] ]

	
	seq = [ [1,0,0,1],
			[0,0,1,1],
			[0,1,1,0],
			[1,1,0,0] ]

	motor = int(sys.argv[1])
	stepFull = 512

	winkel = float(sys.argv[2])
	

	if (motor == 1):
		ControlPin = ControlPin1
	elif (motor == 2):
		ControlPin = ControlPin2
	elif (motor == 3):
		ControlPin = ControlPin3
		
	for pin in ControlPin:
		GPIO.setup(pin, GPIO.OUT)
		GPIO.output(pin, 0)
		
	if (winkel < 0):
		winkel = int(abs(winkel))
		seq = seql
	
	steps = int(float(winkel) / 360 * stepFull)
	for i in range(steps):
		for halfstep in range(4):
			for pin in range(4):
				GPIO.output(ControlPin[pin], seq[halfstep][pin])
			time.sleep(0.005)
	GPIO.cleanup()
