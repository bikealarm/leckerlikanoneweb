#!/usr/bin/python

import StepperFunctions as Step
import sys

if (len(sys.argv) == 2):
	winkel = float(sys.argv[1])
	Step.turnStepperAngle("Turner", winkel)
else: 
	print "Please enter only the angle. "

