#! /usr/bin/python

import RPi.GPIO as GPIO
import time
import sys

if (len(sys.argv) == 2):
	input = float(sys.argv[1]) #Schussstaerke uebergeben 
	Schussweite = 7.5 - input * 3

	servoPINdropper = 11 # verwendete Pins
	servoPINspanner = 8
	servoPINhalter = 7
	
	GPIO.setmode(GPIO.BOARD)
	GPIO.setup(servoPINdropper, GPIO.OUT)
	GPIO.setup(servoPINspanner, GPIO.OUT)
	GPIO.setup(servoPINhalter, GPIO.OUT)
	
	dropper = GPIO.PWM(servoPINdropper, 50) # Frequenz = 50Hz
	halter = GPIO.PWM(servoPINhalter, 50)
	spanner = GPIO.PWM(servoPINspanner, 50)
	
	dropper.start(5) # Initialisierung
	time.sleep(0.5)
	
	# Nachladen
	dropper.ChangeDutyCycle(2.0) 
	time.sleep(0.5)
	dropper.ChangeDutyCycle(5) # zurueck auf Ausgangsposition
	time.sleep(0.5)
	dropper.stop()
	
	#Spannen
	halter.start(5)
	time.sleep(0.5)
	halter.ChangeDutyCycle(2.5) # Wurfarm festhalten
	time.sleep(0.5)
	halter.ChangeDutyCycle(0)
	spanner.start(12.5)
	time.sleep(1)
	spanner.ChangeDutyCycle(Schussweite) # Gummi spannen
	time.sleep(1)
	
	#Abschuss!
	halter.ChangeDutyCycle(5) # Wurfarm loslassen
	time.sleep(0.5)
	halter.stop()
	
	spanner.ChangeDutyCycle(12.5) # Spanner zurueck auf Ausgang
	time.sleep(1)
	spanner.stop()
	
	GPIO.cleanup()
