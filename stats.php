<?php

require_once 'classes/Membership.php';
$membership = New Membership();

$membership->confirm_Member();

?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="height = 960, width = 1000px user-scalable = yes">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/pure-min.css">
</head>
<body>
<div id="main">
	<div id="header">
		<div id="logo">
			<img src="images/header.png">
		</div>
		<div id="nav">
			<ul id="menu">
			  <!-- put class="selected" in the li tag for the selected page - to highlight which page you're on -->
			  <li><a href="index.php">Home</a></li>
			  <li><a href="settings.php">Settings</a></li>
			  <li class="selected"><a href="stats.php">Stats</a></li>
			  <li><a href="about.php">About</a></li>
			  <li><a href="help.php">Help</a></li>
			  <li><a href="login.php?status=loggedout">Log Out</a></li>
			</ul>
		</div>
	</div>


	<div id="content">
		<div class = "sidebar">
			
		</div>
		<div id="content_text">
			<?php
				require_once 'classes/Mysql.php';
				$mysql = New Mysql();
				$list = $mysql->getShotList();
				if ($list->num_rows > 0) 
				{
					echo "<table style=\"width:100%\"> <thead><tr><th>Name</th><th>Anzahl der Schüsse</th></tr></thead> <tbody>";
						// output data of each row
					while($row = $list->fetch_assoc()) 
					{
						echo "<tr><td>".$row["username"]."</td><td>".$row["shots"]."</td></tr>";
					}
						echo "</tbody></table>";
				} else {
					echo "0 results";
				}
			?>
		</div>
		
		
	</div>
	<div id="content_footer"></div>
		<div id="footer">
				<a href="http://validator.w3.org/check?uri=referer">HTML5</a> |
				<a href="http://jigsaw.w3.org/css-validator/check/referer">CSS</a> |
				<a href="http://www.html5webtemplates.co.uk">design from HTML5webtemplates.co.uk</a>
		
			<br><br>
			<p class="right">Teammitglieder: Timo Weber | Tobias Kalb</p>
		</div>
	</div>
</div>

</body>
</html>

