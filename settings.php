<?php

require_once 'classes/Membership.php';
$membership = New Membership();

$membership->confirm_Member();

require_once 'classes/Mysql.php';

if(isset($_POST["submit_add"]))
{
	//add User Code hier einfügen
	$name_add=$_POST['name_add'];
	$password_add=$_POST['password_add'];
	$rpt_password_add=$_POST['rpt_password_add'];
	$mysql = New Mysql();
	if(strlen($password_add) >= 5)
	{
		if($password_add == $rpt_password_add)
		{
			if(strlen($name_add) >= 3 and strlen($name_add) <= 20)
			{
				$nameAvailable = $mysql->usernameAvailable($name_add);
				if($nameAvailable)
				{
					$mysql->addUser($name_add, md5($password_add));
					$success_add = "Benutzer wurde erfolgreich hinzugefügt.";
				} else {
					$error_username_add = "Benutzername ist schon vergeben.";
				}
			} else {
				$error_username_add = "Der Benutzername muss zwischen 3 und 20 Zeichen lang sein.";
			}
			
		} else {
			$error_new_password_add = "Passwörter stimmen nicht überein.";
		}

	} else {
		$error_new_password_add = "Das neue Passwort muss mindestens 5 Zeichen lang sein.";
	}
}
if(isset($_POST["submit_change"]))
{
	$old_password_change=$_POST['old_password_change'];
	$password_change=$_POST['password_change'];
	$rpt_password_change=$_POST['rpt_password_change'];
	$mysql = New Mysql();
	if(strlen($password_change) >= 5)
	{
		if($password_change == $rpt_password_change)
		{
			$ensure_credentials = $mysql->verify_Username_and_Pass($_SESSION['username'], md5($old_password_change));
			if($ensure_credentials) 
			{
				$mysql->changePassword($_SESSION['username'], md5($password_change));
				$success_change = "Passwort wurde erfolgreich geändert.";
			} else {
				$error_user_add = "Benutzername ist bereits vergeben.";
			}
			
		} else {
			$error_new_password_add = "Passwörter stimmen nicht überein.";
		}
	} else {
		$error_new_password_add = "Das neue Passwort muss mindestens 5 Zeichen lang sein.";
	}

}  

?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="height = 480, width = 600px user-scalable = yes">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/pure-min.css">


</head>
<body>
<div id="main">
	<div id="header">
		<div id="logo">
			<img src="images/header.png">
		</div>
		<div id="nav">
			<ul id="menu">
			  <!-- put class="selected" in the li tag for the selected page - to highlight which page you're on -->
			  <li><a href="index.php">Home</a></li>
			  <li class="selected"><a href="settings.php">Settings</a></li>
			  <li><a href="stats.php">Stats</a></li>
			  <li><a href="about.php">About</a></li>
			  <li><a href="help.php">Help</a></li>
			  <li><a href="login.php?status=loggedout">Log Out</a></li>
			</ul>
		</div>
	</div>


	<div id="content">
		<div id="content_form">
			<form class="pure-form pure-form-aligned" method="post">
				<fieldset>
					<legend>Einen neuen Benutzer hinzufügen</legend>
					<div class="pure-control-group">
						<label for="name">Username</label>
						<input class="pure-input-2-5" name="name_add" type="text" placeholder="Username">
						<?php echo $error_username_add; ?>
					</div>

					<div class="pure-control-group">
						<label for="password">Passwort</label>
						<input class="pure-input-2-5" name="password_add" type="password" placeholder="Password">
						<?php echo $error_new_password_add; ?>
					</div>
				    <div class="pure-control-group">
						<label for="password">Passwort bestätigen</label>
						<input class="pure-input-2-5" name="rpt_password_add" type="password" placeholder="Passwort bestätigen">
						<?php echo $error_new_password_add; ?>
					</div>
					 <div class="pure-controls">
						<button type="submit" name="submit_add" class="pure-button pure-button-primary">Neuen Benutzer anlegen</button>
						<?php echo $success_add; ?>
					</div>
				</fieldset>
			</form>
			
			<form class="pure-form pure-form-aligned" method="post">
				<fieldset>
					<legend>Passwort ändern</legend>
					<div class="pure-control-group">
						<label for="name">Altes Passwort</label>
						<input class="pure-input-2-5" name="old_password_change" type="password" placeholder="Altes Passwort">
						<?php echo $error_old_password_change; ?>
					</div>

					<div class="pure-control-group">
						<label for="password">Neues Passwort</label>
						<input class="pure-input-2-5" name="password_change" type="password" placeholder="Password">
						<?php echo $error_new_password_change; ?>
					</div>
				    <div class="pure-control-group">
						<label for="password">Passwort bestätigen</label>
						<input class="pure-input-2-5" name="rpt_password_change" type="password" placeholder="Passwort wiederholen">
					</div>
					 <div class="pure-controls">
						<button type="submit" name="submit_change" class="pure-button pure-button-primary">Passwort ändern</button>
						<?php echo $success_change; ?>
					</div>
				</fieldset>
			</form>
				
		</div>
		
		
	</div>
	<div id="content_footer"></div>
		<div id="footer">
				<a href="http://validator.w3.org/check?uri=referer">HTML5</a> |
				<a href="http://jigsaw.w3.org/css-validator/check/referer">CSS</a> |
				<a href="http://www.html5webtemplates.co.uk">design from HTML5webtemplates.co.uk</a>
		
			<br><br>
			<p class="right">Teammitglieder: Timo Weber | Tobias Kalb</p>
		</div>
	</div>
</div>

</body>
</html>
