<?php

require_once('/var/www/html/includes/constants.php');
class Mysql {
	private $conn;
	
	function __construct() {
		$this->conn = new mysqli(DB_SERVER, DB_USER, DB_PASSWORD, DB_NAME) or 
					  die('There was a problem connecting to the database.');
	}
	
	function verify_Username_and_Pass($un, $pwd) {
				
		$query = "SELECT *
				FROM users
				WHERE username = ? AND password = ?
				LIMIT 1";
				
		if($stmt = $this->conn->prepare($query)) {
			$stmt->bind_param('ss', $un, $pwd);
			$stmt->execute();
			
			if($stmt->fetch()) {
				$stmt->close();
				return true;
			}
		}
		
	}

	function getShotList() {
		
		$sql = "SELECT username, shots FROM users";
		$result = $this->conn->query($sql);
	return $result;

	}
	
	function addShot($un) {
		
		$query = "UPDATE users SET shots= shots + 1 WHERE username=?";
		if($stmt = $this->conn->prepare($query)) {
			$stmt->bind_param('s', $un);
			$stmt->execute();
		}

	}
	
	function changePassword($un, $pwd) {
		$query = "UPDATE users SET password= ? WHERE username=?";
		if($stmt = $this->conn->prepare($query)) {
			$stmt->bind_param('ss', $pwd,$un);
			$stmt->execute();
		}
	}
	
	function usernameAvailable($un) {
		$query = "SELECT *
		FROM users
		WHERE username = ?
		LIMIT 1";
				
		if($stmt = $this->conn->prepare($query)) {
			$stmt->bind_param('s', $un);
			$stmt->execute();
			
			if($stmt->fetch()) {
				$stmt->close();
				return false;
			} else {
				return true;
			}
		}
	}
	
	function addUser($un, $pwd) {
		$query = "INSERT INTO users (username, password)
		VALUES (?, ?)";

		if($stmt = $this->conn->prepare($query)) {
			$stmt->bind_param('ss', $un, $pwd);
			$stmt->execute();
		}
		
	}
}
?>